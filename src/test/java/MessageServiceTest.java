import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class MessageServiceTest {
    @Autowired
    MessageService messageService;

    @MockBean
    Network network;

    @Test
    public void sendMessageIfIpDoesntExistsOrServerIsDownShouldReturnException() throws Exception{
        String ip ="4334";
        String message = "geia";
        assertEquals(false,messageService.sendMessage(ip,message));
    }

    @Test
    public void sendMessageIfIpExistsShouldReturnTrue() throws Exception{
        Network network = new Network();
        String ip ="4334";
        String message = "geia";
        network.setIp(ip);
        network.setMessage(message);
        assertEquals(true,messageService.sendMessage(ip,message));
    }
    @Test(expected = NullPointerException.class)
    public void sendMessageIfIpIsNullShouldReturnTrue() throws Exception {
        Network network = new Network();
        String ip =null;
        String message = "geia";
        network.setIp(ip);
        network.setMessage(message);
        fail("ip is null");
    }
}