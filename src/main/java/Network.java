public class Network {

    private String ip;
    private String message;

    public String getIp() {
        return ip;
    }

    public String getMessage() {
        return message;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public boolean sendMessage(String ip,String message){
        if(ip==getIp()){
            setMessage(message);
            return true;
        }
        return false;

    }
}